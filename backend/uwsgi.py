import os, sys
from werkzeug.wsgi import DispatcherMiddleware

from app.factory import create_app

app = create_app()

# application = DispatcherMiddleware(app)
